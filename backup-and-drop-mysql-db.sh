#! /bin/bash
#Author : Dharmesh Pandav (MrPandav)
# Description : This script will find all user-created databaes granted to user , take backup of it and the drop it

TIMESTAMP=$(date +"%F")
BACKUP_DIR="backup/$TIMESTAMP"
MYSQL_USER="username"
MYSQL=/usr/bin/mysql
MYSQL_PASSWORD="password"
MYSQLDUMP=/usr/bin/mysqldump
MYSQLADMIN=/usr/bin/mysqladmin
mkdir -p "$BACKUP_DIR/mysql"

databases=`$MYSQL --user=$MYSQL_USER -p$MYSQL_PASSWORD -e "SHOW DATABASES;" | grep -Ev "(Database|information_schema|performance_schema)"`

for db in $databases; do
    if [ "$db" != 'performance_schema' ]  && [ "$db" != 'phpmyadmin' ]  && [ "$db" != 'mysql' ] && [ "$db" != "information_schema" ]; then
       $MYSQLDUMP --force --opt --user=$MYSQL_USER -p$MYSQL_PASSWORD --databases $db | gzip > "$BACKUP_DIR/mysql/$db.gz"
       $MYSQLADMIN --user=$MYSQL_USER -p$MYSQL_PASSWORD -f drop $db
    fi
done
