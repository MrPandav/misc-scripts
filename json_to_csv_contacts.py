"""
__author__      = "Dharmesh Pandav
__about__       = "http://blog.privatenode.in/about/"
__linked_in__   = "https://www.linkedin.com/in/dharmeshpandav"
__upwork__      = "https://www.upwork.com/o/profiles/users/_~01d7915baec685ec52/"
__paypal__      = "https://www.paypal.me/DharmeshPandav"
__copyright__   = "feel free to use this piece of code, just don't forget to give credit where it's due ( to me)"
__license__     = "Public Domain"
__version__     = "1.0"
__description__ = script to convert cm-backup contact json file to google compatible csv file
"""
import os
import csv

# CM backup json data structure
data = {
    "A": [
        {
            "group_name": "ABC",
            "phone": [
                {
                    "number": "333333333",
                    "type": "3"
                },
                {
                    "number": "4444444444444444",
                    "type": "3"
                }
            ],
            "email": [
                {
                    "number": "efg@efg.com",
                    "type": "3"
                },
                {
                    "number": "wxf@wxf.com",
                    "type": "3"
                }
            ],
            "addr": "",
            "name": "A-Hello World-",
            "key": "some-random-key-by-cm",
            "avatar": ""
        },
        {
            "group_name": "DEF",
            "phone": [
                {
                    "number": "6666666666",
                    "type": "3"
                },
                {
                    "number": "7777777777777",
                    "type": "3"
                }
            ],
            "email": [
                {
                    "number": "jjjjjjj@qweqwe.com",
                    "type": "3"
                },
                {
                    "number": "kkkkkkkkkkkkkk@ooooooo.com",
                    "type": "3"
                }
            ],
            "addr": "",
            "name": "A- James Bond- 02",
            "key": "some-random-key-by-cm",
            "avatar": ""
        },
    ],
    "B": [
        {
            "group_name": "Bsdasd",
            "phone": [
                {
                    "number": "11111111111",
                    "type": "3"
                },
                {
                    "number": "2222222222222",
                    "type": "3"
                }
            ],
            "email": [
                {
                    "number": "abc@abc.com",
                    "type": "3"
                },
                {
                    "number": "xyz@xyz.com",
                    "type": "3"
                }
            ],
            "addr": "",
            "name": "B- Hello World",
            "key": "some-random-key-by-cm",
            "avatar": ""
        },
    ],
}

# Google Contact CSV structure template
template = {
    'Name': "", 'Given Name': "", 'Additional Name': "", 'Family Name': "", 'Yomi Name': "", 'Given Name Yomi': "",
    'Additional Name Yomi': "", 'Family Name Yomi': "", 'Name Prefix': "", 'Name Suffix': "", 'Initials': "",
    'Nickname': "", 'Short Name': "", 'Maiden Name': "", 'Birthday': "", 'Gender': "", 'Location': "",
    'Billing Information': "", 'Directory Server': "", 'Mileage': "", 'Occupation': "", 'Hobby': "", 'Sensitivity': "",
    'Priority': "", 'Subject': "", 'Notes': "", 'Group Membership': "", 'E-mail 1 - Type': "", 'E-mail 1 - Value': "",
    'E-mail 2 - Type': "", 'E-mail 2 - Value': "", 'E-mail 3 - Type': "", 'E-mail 3 - Value': "", 'IM 1 - Type': "",
    'IM 1 - Service': "", 'IM 1 - Value': "", 'Phone 1 - Type': "", 'Phone 1 - Value': "", 'Phone 2 - Type': "",
    'Phone 2 - Value': "", 'Address 1 - Type': "", 'Address 1 - Formatted': "", 'Address 1 - Street': "",
    'Address 1 - City': "", 'Address 1 - PO Box': "", 'Address 1 - Region': "", 'Address 1 - Postal Code': "",
    'Address 1 - Country': "", 'Address 1 - Extended Address': "", 'Organization 1 - Type': "",
    'Organization 1 - Name': "", 'Organization 1 - Yomi Name': "", 'Organization 1 - Title': "",
    'Organization 1 - Department': "", 'Organization 1 - Symbol': "", 'Organization 1 - Location': "",
    'Organization 1 - Job Description': "", 'Website 1 - Type': "",
    'Website 1 - Value': "", 'Website 2 - Type': "", 'Website 2 - Value': ""}


def master():
    filename = "cm_contact2.csv"
    try:
        os.remove(filename)
    except OSError:
        pass
    with open(filename, "wb") as myfile:
        dict_writer = csv.DictWriter(myfile, fieldnames=[k for k, v in template.iteritems()])
        dict_writer.writeheader()
        for da in data:
            for d in data[da]:
                contact = {}
                contact['Group Membership'] = d['group_name']
                counter = 1
                for phone in d['phone']:
                    contact['Phone %s - Type' % counter] = 'mobile'
                    contact['Phone %s - Value' % counter] = phone['number']
                    counter += 1
                counter = 1
                for email in d['email']:
                    contact['E-mail %s - Type' % counter] = 'personal'
                    contact['E-mail %s - Value' % counter] = email['number']
                    counter += 1
                contact['Name'] = d['name']
                contact['Given Name'] = d['name']
                dict_writer.writerow(contact)


if __name__ == '__main__':
    master()
