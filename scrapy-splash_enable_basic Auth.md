we are running a scrapy-splash instance , where we have HTTP Auth is in place to grant access to splash instance...

after surfing and reading for a while i came to know that there is no HTTP Auth for Scrapy-splash API..

I tried using something like http://username:pass@splash.instance.url.com
but scrapy failed with error DNS "username" is not resolved

after that i have started hacking into packages in scrapyjs ( midddleware.py) and came to a realization that it can be done ,very efficiently with minimal changes in code-base
```python
#in Settings.py file///
DOWNLOADER_MIDDLEWARES = {
    'scrapyjs.SplashMiddleware': 725,
}
SPLASH_URL = 'URL'
SPLASH_USERNAME = 'username'
SPLASH_PASS = 'password'

#in scrapyjs.middleware.py file
from w3lib.http import basic_auth_header

class SplashMiddleware(object):
    .....
    default_splash_username = ""
    default_splash_password = ""
    .....
def __init__(self, crawler, splash_base_url, slot_policy):
        ....
        self.splash_username = crawler.settings.get('SPLASH_USERNAME', self.default_splash_username)
        self.splash_password = crawler.settings.get('SPLASH_PASS', self.default_splash_password)

def process_request(self, request, spider):
       if self.splash_username != "" and self.default_splash_password != "":
            auth = basic_auth_header(http_user, http_pass)
            request.headers['Authorization'] = auth
```
i think this will hlep many who have HTTP Auth in place for splash instance
