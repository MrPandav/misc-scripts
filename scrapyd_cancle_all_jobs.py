# Author : Dharmesh Pandav (MrPandav)
# Simple Python script to cancel all pending jobs on scrapyd instance
# python file call format
# python scrapyd_cancle_pendding_job.py projectname ip port

import sys
import requests
JOB_LIST_URL = "http://%s:%s/listjobs.json?project=%s"
JOB_CANCLE_URL = "http://%s:%s/cancel.json"

def cancle_pending_jobs(projectname="default", ip="localhost", port="6800"):

    response = requests.get(JOB_LIST_URL%(ip, port, projectname))
    data = response.json()

    for job in data['pending']:
        print job['id']
        params = {'project': projectname ,"job": job['id']}
        requests.post(JOB_CANCLE_URL%(ip,port), data=params, allow_redirects=True)

    for job in data['running']:
        print job['id']
        params = {'project': projectname ,"job": job['id']}
        requests.post(JOB_CANCLE_URL%(ip,port), data=params, allow_redirects=True)


if __name__ == "__main__":
    cancle_pending_jobs(sys.argv[1],sys.argv[2],sys.argv[3])
